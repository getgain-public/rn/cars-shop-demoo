import React from 'react';
import { Provider } from 'react-redux';
import Navigation from './modules/navigation';
import store from './modules/store/store';

const App = () => {
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
};

export default App;
