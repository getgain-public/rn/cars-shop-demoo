import { createSlice } from '@reduxjs/toolkit';
import {
  setCars,
  setNextPage,
  setSelectedCar,
} from '@src/modules/cars/store/actions';
import { Car } from '@src/modules/cars/domain/interfaces/Car';

type State = {
  carsList: Car[];
  selectedCar: null | Car;
  isCarsLoading: boolean;
  isPageLoading: boolean;
};

const carsSlice = createSlice({
  name: 'cars',
  initialState: {
    carsList: [],
    selectedCar: null,
    isCarsLoading: false,
    isPageLoading: false,
  } as State,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(setCars.fulfilled, (state: State, { payload }) => {
      state.carsList = payload;
    });
    builder.addCase(setSelectedCar.fulfilled, (state: State, { payload }) => {
      state.selectedCar = payload;
    });
    builder.addCase(setNextPage.pending, (state: State) => {
      state.isPageLoading = true;
    });
    builder.addCase(setNextPage.fulfilled, (state: State, { payload }) => {
      state.carsList = payload;
      state.isPageLoading = false;
    });
    builder.addCase(setNextPage.rejected, (state: State) => {
      state.isPageLoading = false;
    });
  },
});

const carsReducer = carsSlice.reducer;
export type CarsStore = ReturnType<typeof carsReducer>;
export default carsReducer;
