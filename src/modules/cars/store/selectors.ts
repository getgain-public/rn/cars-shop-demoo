import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '@src/modules/store/store';

const carsStateSelector = (state: RootState) => state.cars;

export const carsListSelector = createSelector(
  carsStateSelector,
  state => state.carsList,
);

export const selectedCarSelector = createSelector(
  carsStateSelector,
  state => state.selectedCar,
);

export const isCarsLoadingSelector = createSelector(
  carsStateSelector,
  state => state.isCarsLoading,
);

export const isPageLoadingSelector = createSelector(
  carsStateSelector,
  state => state.isPageLoading,
);
