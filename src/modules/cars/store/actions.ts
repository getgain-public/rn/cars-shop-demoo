import { createAsyncThunk } from '@reduxjs/toolkit';
import carService from '@src/modules/cars/domain/services/CarService';
import { Car } from '@src/modules/cars/domain/interfaces/Car';
import { RootState } from '@src/modules/store/store';
import { decorateUiKeys } from '@src/modules/cars/domain/helpers/decorateUiKeys';

const PREFIX = 'cars';

export const setCars = createAsyncThunk(`${PREFIX}/setCars`, () =>
  carService.loadCars(),
);

export const setSelectedCar = createAsyncThunk(
  `${PREFIX}/setSelectedCar`,
  (selectedCar: Car) => selectedCar,
);

export const setNextPage = createAsyncThunk(
  `${PREFIX}/setNextPage`,
  async (_, thunkAPI) => {
    const state: RootState = thunkAPI.getState();
    let prevCarsList = state.cars.carsList;
    const carsList = await carService.loadCars();
    return decorateUiKeys(prevCarsList.concat(carsList));
  },
);
