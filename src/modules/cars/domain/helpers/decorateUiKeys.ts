import 'react-native-get-random-values';
import { nanoid } from 'nanoid';

export function decorateUiKeys<Key = '_uiKey'>(
  array: any[],
  keyName = '_uiKey',
) {
  return array.map(element => ({
    ...element,
    [keyName]: nanoid(6),
  }));
}
