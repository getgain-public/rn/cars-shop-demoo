import styled from '@emotion/native';

type Props = {
  fontSize: number;
};

export const UDText = styled.Text`
  font-size: ${(props: Props) => `${props.fontSize}px`};
  color: white;
  opacity: 0.9;
`;
