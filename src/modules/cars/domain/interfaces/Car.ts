export interface Car {
  _uiKey: string;
  manufacturer: string;
  model: string;
  year: number;
  color: string;
  country: string;
  price: string;
  description: string;
}
