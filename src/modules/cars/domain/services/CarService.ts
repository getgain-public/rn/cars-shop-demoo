import { BaseRepository } from '@snap-alex/domain-js';
import { Car } from '@src/modules/cars/domain/interfaces/Car';
import carsRepository from '@src/modules/cars/domain/repositories/CarsRepository';
import { decorateUiKeys } from '@src/modules/cars/domain/helpers/decorateUiKeys';

class CarService {
  constructor(private repository: BaseRepository) {}

  public async loadCars(): Promise<Car[]> {
    const response = await this.repository.load();
    return decorateUiKeys(response);
  }
}

const carService = new CarService(carsRepository);
export default carService;
