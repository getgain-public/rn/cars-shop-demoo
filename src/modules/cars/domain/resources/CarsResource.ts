import { BaseRestResource } from '@snap-alex/domain-js';
import httpResource from '@src/modules/core/infrastructure/httpResource';

const resource = '6e8d9371-933d-4cbc-bef6-8f626c97083b';

class CarsResource extends BaseRestResource {}

const carsResource = new CarsResource(httpResource, resource);
export default carsResource;
