import { BaseRepository } from '@snap-alex/domain-js';
import { Car } from '@src/modules/cars/domain/interfaces/Car';
import carsResource from '@src/modules/cars/domain/resources/CarsResource';

export class CarsRepository extends BaseRepository<Car> {
  public async get(): Promise<Car> {
    return this.resource().get();
  }
}

const carsRepository = new CarsRepository(carsResource);
export default carsRepository;
