import React, { useEffect } from 'react';
import * as S from './styles';
import HomeComponentsCarsList from '@src/modules/cars/ui/components/cars-list';
import { useDispatch } from 'react-redux';
import { setCars } from '@src/modules/cars/store/actions';
import UDHeader from '@src/modules/ud-ui/components/ud-header';
import screenNames from '@src/modules/navigation/screen-names';

export default function CarsScreen() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setCars());
  }, [dispatch]);

  return (
    <S.Container>
      <UDHeader screenName={screenNames.cars} />
      <HomeComponentsCarsList />
    </S.Container>
  );
}
