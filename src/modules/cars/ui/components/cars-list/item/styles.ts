import styled from '@emotion/native';
import FastImage from 'react-native-fast-image';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height;

export const Container = styled.TouchableOpacity`
  height: ${`${height / 4}px`};
  margin: 2%;
  border-radius: 15px;
`;

export const LabelContainer = styled.View`
  margin: 3% 0 0 3%;
`;

export const Image = styled(FastImage)`
  flex: 1;
  border-radius: 15px;
`;
