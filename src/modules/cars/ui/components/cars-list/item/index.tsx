import React from 'react';
import { Car } from '@src/modules/cars/domain/interfaces/Car';
import * as S from './styles';
import { UDText } from '@src/modules/cars/domain/helpers/UDText';

type Props = {
  car: {
    item: Car;
    index: number;
  };
  onPress: (car: Car) => void;
};

export default function HomeComponentsCarsListItem({ car, onPress }: Props) {
  const fakeImage = 'https://www.hyundai.ru/images/common/header/slider5.png';
  return (
    <S.Container onPress={() => onPress(car)}>
      <S.Image source={{ uri: fakeImage }}>
        <S.LabelContainer>
          <UDText fontSize={24}>{car.item.manufacturer}</UDText>
          <UDText fontSize={12}>{car.item.model}</UDText>
        </S.LabelContainer>
      </S.Image>
    </S.Container>
  );
}
