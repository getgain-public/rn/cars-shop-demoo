import React, { useCallback, useRef } from 'react';
import { Dimensions, FlatList } from 'react-native';
import * as S from './styles';
import { useDispatch, useSelector } from 'react-redux';
import {
  carsListSelector,
  isCarsLoadingSelector,
  isPageLoadingSelector,
} from '@src/modules/cars/store/selectors';
import HomeComponentsCarsListItem from '@src/modules/cars/ui/components/cars-list/item';
import UDModalize from '@src/modules/ud-ui/components/ud-modalize';
import HomeComponentsModalizeDetails from '@src/modules/cars/ui/components/modalize-details';
import { Modalize } from 'react-native-modalize';
import {
  setCars,
  setNextPage,
  setSelectedCar,
} from '@src/modules/cars/store/actions';

const height = Dimensions.get('window').height;

export default function HomeComponentsCarsList() {
  const dispatch = useDispatch();
  const carsList = useSelector(carsListSelector);
  const isCarsLoading = useSelector(isCarsLoadingSelector);
  const isPageLoading = useSelector(isPageLoadingSelector);
  const modalizeRef = useRef<Modalize>(null);
  const flatlistRef = useRef(null);

  const onPress = useCallback(
    car => {
      dispatch(setSelectedCar(car.item));
      scrollToItem(car.index);
      modalizeRef.current?.open();
    },
    [dispatch],
  );

  const renderItem = useCallback(
    car => <HomeComponentsCarsListItem car={car} onPress={onPress} />,
    [onPress],
  );

  const scrollToItem = useCallback(index => {
    flatlistRef.current?.scrollToIndex({ animated: true, index: index });
  }, []);

  const handleLoadMore = useCallback(() => {
    dispatch(setNextPage());
  }, [dispatch]);

  const handleFooter = useCallback(
    () => (isPageLoading ? <S.LoadingIndicator size={30} /> : null),
    [isPageLoading],
  );

  const keyExtractor = useCallback(item => item._uiKey, []);

  const updateCars = useCallback(() => dispatch(setCars()), [dispatch]);

  return (
    <S.Container>
      <FlatList
        ref={flatlistRef}
        data={carsList}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        showsVerticalScrollIndicator={false}
        onEndReachedThreshold={0}
        onEndReached={handleLoadMore}
        ListFooterComponent={handleFooter}
        onRefresh={updateCars}
        refreshing={isCarsLoading}
      />
      <UDModalize modalizeRef={modalizeRef} modalHeight={height / 1.62}>
        <HomeComponentsModalizeDetails />
      </UDModalize>
    </S.Container>
  );
}
