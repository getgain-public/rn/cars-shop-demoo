import styled from '@emotion/native';
import { UDText } from '@src/modules/cars/domain/helpers/UDText';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height;

export const Container = styled.View`
  height: ${`${height / 1.62}px`};
  background-color: lightgray;
`;

export const DescriptionContainer = styled.View`
  padding: 6%;
`;

export const TitleContainer = styled.View`
  align-self: center;
  margin-top: 4%;
`;

export const DetailsContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 5% 10% 0 10%;
`;

export const DetailsListContainer = styled.View`
  align-self: center;
`;

export const Label = styled(UDText)`
  color: black;
`;
