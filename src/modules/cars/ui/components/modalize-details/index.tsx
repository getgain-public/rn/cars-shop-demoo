import React from 'react';
import * as S from './styles';
import { useSelector } from 'react-redux';
import { selectedCarSelector } from '@src/modules/cars/store/selectors';

export default function HomeComponentsModalizeDetails() {
  const selectedCar = useSelector(selectedCarSelector);
  return (
    <S.Container>
      <S.TitleContainer>
        <S.Label fontSize={32}>{selectedCar?.model}</S.Label>
      </S.TitleContainer>
      <S.DetailsContainer>
        <S.Label fontSize={16}>Color: {selectedCar?.color}</S.Label>
        <S.Label fontSize={16}>Country: {selectedCar?.country}</S.Label>
      </S.DetailsContainer>
      <S.DetailsContainer>
        <S.Label fontSize={16}>Year: {selectedCar?.year}</S.Label>
        <S.Label fontSize={16}>Price: {selectedCar?.price}</S.Label>
      </S.DetailsContainer>
      <S.DescriptionContainer>
        <S.Label fontSize={14}>{selectedCar?.description}</S.Label>
      </S.DescriptionContainer>
    </S.Container>
  );
}
