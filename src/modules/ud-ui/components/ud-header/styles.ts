import styled from '@emotion/native';

export const Container = styled.View`
  height: 12%;
  background-color: darkslateblue;
  border-radius: 0 0 300px 300px;
  justify-content: center;
  align-items: center;
  overflow: hidden;
`;

export const Label = styled.Text`
  font-size: 32px;
  color: white;
  padding-top: 5%;
`;
