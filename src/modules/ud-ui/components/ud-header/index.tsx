import React from 'react';
import * as S from './styles';

type Props = {
  screenName: string;
};

export default function UDHeader({ screenName }: Props) {
  return (
    <S.Container>
      <S.Label>{screenName}</S.Label>
    </S.Container>
  );
}
