export const ModalStyle = {
  borderTopLeftRadius: 40,
  borderTopRightRadius: 40,
  overflow: 'hidden',
};
