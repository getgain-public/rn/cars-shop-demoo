import React from 'react';
import * as S from './styles';
import { Modalize, ModalizeProps } from 'react-native-modalize';
import { RefObject } from 'react';

type Props = {
  modalizeRef: RefObject<Modalize>;
  children: JSX.Element;
  onCloseEnd?: (...args: any) => any;
} & Omit<ModalizeProps, 'ref' | 'HeaderComponent' | 'children'>;

export default function UDModalize(props: Props) {
  const { children, onCloseEnd, modalizeRef, ...modalizeProps } = props;
  return (
    <Modalize
      ref={modalizeRef}
      onClose={onCloseEnd}
      withHandle={false}
      modalStyle={S.ModalStyle}
      overlayStyle={{ position: 'relative' }}
      scrollViewProps={{ bounces: false }}
      {...modalizeProps}>
      {children}
    </Modalize>
  );
}
