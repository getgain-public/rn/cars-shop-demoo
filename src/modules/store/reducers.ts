import carsReducer from '@src/modules/cars/store';

export default {
  cars: carsReducer,
};
